package practica3.brayanpincayv.facci.practica3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class ActividadPrincipal extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(ActividadPrincipal.this,ActividadLogin.class);
                startActivity(intent);
                break;

            case R.id.opcionRegistar:
                intent = new Intent(ActividadPrincipal.this,ActividarRegistar.class);
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}